package com.example.calendareventreserve

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_INSERT
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.CalendarContract
import android.speech.RecognizerIntent
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.util.Log
import com.example.calendareventreserve.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar
import java.util.*

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    lateinit var tts: TextToSpeech
    lateinit var speech: MutableList<String>
    var TTS = ""
    var mostRecentUtteranceID = ""
    val VOICE_RECOGNITION = 2 //Id for Intent
    var counter = 0
    val limit = 7
    var channel_id = "notification_id"
    var notification_id = 1
    var notification_time: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        speech = mutableListOf()

        initNotification()

        initRecognizer()

        binding.btnReserve.setOnClickListener{
            initTTS()
        }

    }

    private fun initRecognizer() {
        //Speech Recognition
        intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak!");
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1); //1 for only 1 result with confidence
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH);
    }

    private fun initTTS() {
        //Text to Speech
        tts = TextToSpeech(this) { i ->
            if (i == TextToSpeech.SUCCESS) {
                Log.d("speech", "==== PASS")
                tts.language = Locale.ENGLISH
                tts.setPitch(1f)
                tts.setSpeechRate(1f)
                //Set unique utterance ID for each utterance
                TTS = "Hello, What is your name?"
                counter = 0
                speakText()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === VOICE_RECOGNITION) {
            if (resultCode === RESULT_OK) {
                var results = data?.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                var confidence = data?.getFloatArrayExtra(RecognizerIntent.EXTRA_CONFIDENCE_SCORES)

                if (results != null) {
                    for( result in results){
                        Log.d("results", result + " " + counter)
                    }
                    speech.add(results[0])
                }

                when(counter){
                    1 -> TTS = "What is the Event Title?"
                    2 -> TTS = "What Day is the Event? In Numbers"
                    3 -> TTS = "What Month is the Event? In Numbers"
                    4 -> TTS = "Specify a Start Time In Integers"
                    //5 -> TTS = "AM or PM?"
                    5 -> TTS = "Specify a End Time In Integers"
                    //7 -> TTS = "AM or PM?"
                    else -> {
                        TTS = "Done! Bye-bye"
                        insertEvent(speech)
                    }
                }

                speakText()
            }
        }
    }

    private fun speakText() {
        counter++
        mostRecentUtteranceID = (Random().nextInt() % 9999999).toString() + ""
        tts.speak(TTS, TextToSpeech.QUEUE_ADD, null, mostRecentUtteranceID)
        tts.setOnUtteranceProgressListener(object : UtteranceProgressListener() {
            override fun onStart(s: String) {}
            override fun onDone(s: String) {
                Log.d("speech", "==== Utterance - Done")
                if ( counter < limit )
                    startActivityForResult(intent, VOICE_RECOGNITION)
            }

            override fun onError(s: String) {}
        })
    }

    private fun mapToReservation(speech: MutableList<String>): Reservation {
        return Reservation(
                name = speech[0],
                title = speech[1],
                day = speech[2].toInt(),
                month = speech[3].toInt(),
                year = 2020,
                startTime = speech[4].toInt(),
                endTime = speech[5].toInt())
    }

    private fun insertEvent(speech: MutableList<String>) {
        counter = 0
        var reservation = mapToReservation(speech)

        reservation.startTime -= 1
        reservation.endTime -= 1
        reservation.month -= 1

        Log.d("results", reservation.toString())

        var start = Calendar.getInstance()

        start.set(reservation.year, reservation.month, reservation.day, reservation.startTime,0)


        var nt_time = Calendar.getInstance()
        nt_time.set(reservation.year, reservation.month, reservation.day - 1, reservation.startTime,0)
        notification_time = nt_time.timeInMillis

        var startTime = start.timeInMillis
        Log.d("startTime", startTime.toString())

        var end = Calendar.getInstance()
        end.set(reservation.year, reservation.month, reservation.day, reservation.endTime,0)
        var endTime = end.timeInMillis



        var intent = Intent(ACTION_INSERT)
        intent.data = CalendarContract.Events.CONTENT_URI

        intent.putExtra(CalendarContract.Events.TITLE, reservation.title)
        Log.d("results", "Start Time: $startTime")
        Log.d("results", "End Time: $endTime")
        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startTime)
        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, endTime)

        if(intent.resolveActivity(packageManager) != null){

            val alarmManager = this.getSystemService(Context.ALARM_SERVICE) as? AlarmManager

            val reminder = Intent(this, NotificationBR::class.java).let { innerIntent ->
                innerIntent.putExtra("EVENT_TITLE", title)
                innerIntent.putExtra("NOTIFICATION_ID", notification_id++)
                PendingIntent.getBroadcast(this, notification_id, innerIntent, 0)
            }
            alarmManager!!.set(AlarmManager.RTC_WAKEUP, notification_time!!, reminder)

            startActivity(intent)
        }
        else{
            Snackbar.make(binding.root,"There is no application that can support this action!", Snackbar.LENGTH_LONG).show()
        }
    }
    private fun initNotification() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Reminder"
            val descriptionText = "Channel Description!"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channel_id, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
    override fun onPause() {
        tts!!.stop()
        super.onPause()
    }

    override fun onDestroy() {
        tts!!.shutdown()
        super.onDestroy()
    }
}