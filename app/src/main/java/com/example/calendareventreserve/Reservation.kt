package com.example.calendareventreserve

data class Reservation(
        var name: String,
        var title : String,
        var day: Int,
        var month: Int,
        var year: Int = 2020,
        var startTime: Int,
        var endTime: Int
)