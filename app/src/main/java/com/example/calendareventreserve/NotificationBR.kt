package com.example.calendareventreserve

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.JobIntentService

class NotificationBR: BroadcastReceiver() {
    override fun onReceive(p0: Context?, p1: Intent?) {
        val title = p1?.getStringExtra("EVENT_TITLE")
        val notificationID = p1?.getIntExtra("NOTIFICATION_ID", 0)
        val serviceIntent = Intent(p0, NotificationIntentService::class.java)
        serviceIntent.putExtra("TITLE_EVENT_CALENDAR", title)
        serviceIntent.putExtra("ID_NOTIFICATION_EVENT_CALENDAR", notificationID)
        JobIntentService.enqueueWork(p0!!, NotificationIntentService::class.java, 5,  serviceIntent)
    }
}
